#include <stdio.h>
#include <stdlib.h>


int gcd(int m,int n){

    int ans;

    if(m%n==0){
        ans=n;
    }
    else
        ans=gcd(n,m%n);
    return ans;

    return 0;
}

int main()
{
    int m;
    int n;
    printf("enter the first number");
    scanf("%d", &m);

    printf("enter the second number");
    scanf("%d", &n);

    printf("GCD of %d and %d is = %d", m, n, gcd(m,n));


    return 0;
}

